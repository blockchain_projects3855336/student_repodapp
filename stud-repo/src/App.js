//Step 2: change the base code to use Component
import React, { Component } from 'react'
import Web3 from 'web3'
import './App.css';

// Step 3: import the ABI
import { STUDENTRECORD_ABI, STUDENTRECORD_ADDRESS } from './abi/config_studentrecord'
import StudentRecord from './components/StudentRecord'

//We replace function with a component for better management of code
class App extends Component {
    //componentDidMount called once on the client after the initial render
    //when the client receives data from the server and before the data is displayed.
    componentDidMount() {
        if (!window.ethereum)
            throw new Error("No crypto wallet found. Please install it.");
        window.ethereum.send("eth_requestAccounts");
        this.loadBlockchainData()
    }
    //create a web3 connection to using a provider, MetaMask to connect to
    //the ganache server and retrieve the first account
    async loadBlockchainData() {
        const web3 = new Web3(Web3.givenProvider || "HTTP://127.0.0.1:7545")
        const accounts = await web3.eth.getAccounts()
        this.setState({ account: accounts[1] })
        //Step 4: load all the lists from the blockchain
        const studentRecord = new web3.eth.Contract(STUDENTRECORD_ABI,
            STUDENTRECORD_ADDRESS)
        //Keep the lists in the current state
        this.setState({ studentRecord })
        //Get the number of records for all list in the blockchain
        const studentCount = await studentRecord.methods.studentsCount().call()
        //Store this value in the current state as well
        this.setState({ studentCount })
        //Use an iteration to extract each record info and store
        //them in an array
        this.setState.students = []
        for (var i = 1; i <= studentCount; i++) {
            const student = await studentRecord.methods.students(i).call()
            this.setState({
                students: [...this.state.students, student]
            })
        }
    }
    //Initialise the variables stored in the state
    constructor(props) {
        super(props)
        this.state = {
            account: '',
            studentCount: 0,
            students: [],
            loading: true,
        }
        this.addStudent = this.addStudent.bind(this)
    }

    addStudent(sid, name) {
        this.setState({ loading: true })
        this.state.studentRecord.methods.addStudent(sid, name)
            .send({ from: this.state.account })
            .once('receipt', (receipt) => {
                this.setState({ loading: false })
                this.loadBlockchainData()
            })
    }

    markGraduated(sid) {
        this.setState({ loading: true })
        this.state.studentList.methods.markGraduated(sid)
            .send({ from: this.state.account })
            .once('receipt', (receipt) => {
                this.setState({ loading: false })
                this.loadBlockchainData()
            })
    }


        // Function to handle checkbox change and invoke MetaMask
        handleCheckboxChange = async (studentId, graduated) => {
            if (!graduated) {
                try {
                    const studentRecordContract = this.state.studentRecord; // Get the contract instance
                    await studentRecordContract.methods.markGraduated(studentId).send({
                        from: this.state.account,
                    });
                    // Once the transaction is confirmed, update the blockchain data
                    this.loadBlockchainData();
                } catch (error) {
                    console.error("Error while sending transaction: ", error);
                }
            } else {
                // If already graduated, no action is required or you can handle differently if needed
            }
        };



    //Display the first account
    render() {
        return (
            <div className="container">
                <h1>Student Record System</h1>
                <p>Your account: {this.state.account}</p>

                <StudentRecord
                    addStudent={this.addStudent} />
                <ul id="studentList" className="list-unstyled">
                    {
                        //This gets the each student from the studentList
                        //and pass them into a function that display the
                        //details of the student
                        this.state.students.map((student, key) => {
                            return (
                                <li className="list-group-item checkbox" key={key}>
                                    <span className="name alert">{student._id}. {student.sid} {student.name}</span>
                                    <input
                                        className="form-check-input"
                                        type="checkbox"
                                        name={student._id}
                                        defaultChecked={student.graduated}
                                        disabled={student.graduated}
                                        // ref={(input) => {
                                        //     this.checkbox = input
                                        // }}
                                        onChange={() => this.handleCheckboxChange(student._id, student.graduated)}
                                    />
                                    <label className="form-check-label" >Graduated</label>
                                </li>
                            )
                        }
                        )
                    }
                </ul>
            </div>

        );
    }
}
export default App;